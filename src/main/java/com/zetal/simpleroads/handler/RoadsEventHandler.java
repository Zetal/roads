package com.zetal.simpleroads.handler;

import java.util.UUID;

import com.zetal.simpleroads.RoadsBlocks;
import com.zetal.simpleroads.RoadsCommonConfigs;
import com.zetal.simpleroads.SimpleRoadsMod;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.tick.EntityTickEvent;

public class RoadsEventHandler {
	private static AttributeModifier ROAD_MOD_SPEED;
    private static AttributeModifier ROAD_MOD_GRAVITY;
	private static final ResourceLocation ROAD_MOD_SPEED_ID = ResourceLocation.fromNamespaceAndPath(SimpleRoadsMod.MODID, "road_speed");
	private static final ResourceLocation ROAD_MOD_GRAVITY_ID = ResourceLocation.fromNamespaceAndPath(SimpleRoadsMod.MODID, "road_gravity");

	@SubscribeEvent
	public void onPlayerRoad(EntityTickEvent.Pre event) {
		Entity entity = event.getEntity();
		if (event.getEntity().tickCount % 5 == 0 && entity instanceof LivingEntity livingEntity && isPlayerOrRiddenByPlayer(livingEntity)) {
			Level world = livingEntity.level();
			BlockPos pos = new BlockPos(Mth.floor(livingEntity.blockPosition().getX()), Mth.floor(livingEntity.blockPosition().getY()), Mth.floor(livingEntity.blockPosition().getZ()));
			BlockState state = world.getBlockState(pos.below());
			boolean isRoad = state.getBlock() == RoadsBlocks.ROAD.get() || state.getBlock() == RoadsBlocks.ROAD_SLAB.get() || state.getBlock() == RoadsBlocks.ROAD_STAIRS.get();
			if (!isRoad) {
				state = world.getBlockState(pos);
				isRoad = state.getBlock() == RoadsBlocks.ROAD.get() || state.getBlock() == RoadsBlocks.ROAD_SLAB.get() || state.getBlock() == RoadsBlocks.ROAD_STAIRS.get();
			}
			AttributeModifier speedModifier = GetSpeedMod();
			if (isRoad && livingEntity.getAttribute(Attributes.MOVEMENT_SPEED).getModifier(speedModifier.id()) == null) {
				livingEntity.getAttribute(Attributes.MOVEMENT_SPEED).addPermanentModifier(speedModifier);
				if(RoadsCommonConfigs.STICKY_STAIRS.get()) {
					livingEntity.getAttribute(Attributes.GRAVITY).addPermanentModifier(GetGravityMod());
				}
			} else if (!isRoad && livingEntity.getAttribute(Attributes.MOVEMENT_SPEED).getModifier(speedModifier.id()) != null) {
				livingEntity.getAttribute(Attributes.MOVEMENT_SPEED).removeModifier(speedModifier);
				livingEntity.getAttribute(Attributes.GRAVITY).removeModifier(GetGravityMod());
			}
		}
	}
	
	private AttributeModifier GetSpeedMod() {
	    if(ROAD_MOD_SPEED == null) {
	        ROAD_MOD_SPEED = new AttributeModifier(ROAD_MOD_SPEED_ID, RoadsCommonConfigs.ROAD_SPEED_BOOST.get(), AttributeModifier.Operation.ADD_VALUE);
	    }
	    return ROAD_MOD_SPEED;
	}
    
    private AttributeModifier GetGravityMod() {
        if(ROAD_MOD_GRAVITY == null) {
            ROAD_MOD_GRAVITY = new AttributeModifier(ROAD_MOD_GRAVITY_ID, RoadsCommonConfigs.STICKY_GRAVITY_BOOST.get(), AttributeModifier.Operation.ADD_VALUE);
        }
        return ROAD_MOD_GRAVITY;
    }
	
	private boolean isPlayerOrRiddenByPlayer(Entity entity) {
		if(entity instanceof Player) {
			return true;
		}
		if(entity.getControllingPassenger() instanceof Player) {
			return true;
		}
		return false;
	}
}
