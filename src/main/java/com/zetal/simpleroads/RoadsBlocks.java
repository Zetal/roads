package com.zetal.simpleroads;

import com.zetal.simpleroads.block.BlockRoad;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.neoforged.neoforge.registries.DeferredBlock;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;

public class RoadsBlocks
{
	public static DeferredRegister.Blocks BLOCK_REGISTRY = DeferredRegister.createBlocks(SimpleRoadsMod.MODID);
	public static final DeferredBlock<BlockRoad> ROAD = BLOCK_REGISTRY.register("road", (registryName) -> new BlockRoad(BlockBehaviour.Properties.ofFullCopy(Blocks.STONE).setId(ResourceKey.create(Registries.BLOCK, registryName))));
    public static final DeferredBlock<StairBlock> ROAD_STAIRS = BLOCK_REGISTRY.register("road_stairs", (registryName) -> new StairBlock(ROAD.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(ROAD.get()).setId(ResourceKey.create(Registries.BLOCK, registryName))));
	public static final DeferredBlock<SlabBlock> ROAD_SLAB = BLOCK_REGISTRY.register("road_slab", (registryName) -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(ROAD.get()).setId(ResourceKey.create(Registries.BLOCK, registryName))));
	
	public static DeferredRegister.Items ITEM_REGISTRY = DeferredRegister.createItems(SimpleRoadsMod.MODID);
	public static final DeferredItem<BlockItem> ITEM_ROAD = ITEM_REGISTRY.registerSimpleBlockItem(ROAD);
	public static final DeferredItem<BlockItem> ITEM_ROAD_STAIRS = ITEM_REGISTRY.registerSimpleBlockItem(ROAD_STAIRS);
	public static final DeferredItem<BlockItem> ITEM_ROAD_SLAB = ITEM_REGISTRY.registerSimpleBlockItem(ROAD_SLAB);
	
	public static DeferredRegister<CreativeModeTab> TAB_REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, SimpleRoadsMod.MODID);
	public static final DeferredHolder<CreativeModeTab, CreativeModeTab> ROADS_TAB = TAB_REGISTRY.register("roads_tab", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + SimpleRoadsMod.MODID + ".roads_tab"))
            // Set icon of creative tab
            .icon(() -> new ItemStack(RoadsBlocks.ITEM_ROAD.get()))
            // Add default items to tab
            .displayItems((featureFlags, output) -> {
                output.accept(RoadsBlocks.ITEM_ROAD.get());
                output.accept(RoadsBlocks.ITEM_ROAD_SLAB.get());
                output.accept(RoadsBlocks.ITEM_ROAD_STAIRS.get());
            }).build());
}
