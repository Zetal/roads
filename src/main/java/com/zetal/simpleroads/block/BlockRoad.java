package com.zetal.simpleroads.block;

import net.minecraft.world.level.block.Block;

public class BlockRoad extends Block
{
	public BlockRoad(Properties baseProperties)
	{
		super(baseProperties.strength(4.0f));
	}
}
