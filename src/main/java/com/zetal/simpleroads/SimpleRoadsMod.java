package com.zetal.simpleroads;

import com.zetal.simpleroads.handler.RoadsEventHandler;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.neoforge.common.NeoForge;

// The value here should match an entry in the META-INF/neoforge.mods.toml file
@Mod(SimpleRoadsMod.MODID)
public class SimpleRoadsMod {
    // Define mod id in a common place for everything to reference
    public static final String MODID = "simpleroads";

    // The constructor for the mod class is the first code that is run when your mod is loaded.
    // FML will recognize some parameter types like IEventBus or ModContainer and pass them in automatically.
    public SimpleRoadsMod(IEventBus modEventBus, ModContainer modContainer) {
        RoadsBlocks.BLOCK_REGISTRY.register(modEventBus);
        RoadsBlocks.ITEM_REGISTRY.register(modEventBus);
        RoadsBlocks.TAB_REGISTRY.register(modEventBus);
        modContainer.registerConfig(ModConfig.Type.COMMON, RoadsCommonConfigs.SPEC, "roads-common.toml");
        NeoForge.EVENT_BUS.register(new RoadsEventHandler());
    }
}
