package com.zetal.simpleroads;

import net.neoforged.neoforge.common.ModConfigSpec;

public class RoadsCommonConfigs {
    public static final ModConfigSpec.Builder BUILDER = new ModConfigSpec.Builder();
    public static final ModConfigSpec SPEC;

    public static final ModConfigSpec.ConfigValue<Double> ROAD_SPEED_BOOST;
    public static final ModConfigSpec.ConfigValue<Boolean> STICKY_STAIRS;
    public static final ModConfigSpec.ConfigValue<Double> STICKY_GRAVITY_BOOST;

    static {
        BUILDER.push("Roads Config");

        ROAD_SPEED_BOOST = BUILDER.comment("How much should the Roads increase player speed? (Default is 0.04. Requires Restart.)")
                .define("Road Speed Boost", 0.04D);
        STICKY_STAIRS = BUILDER.comment("Should gravity be increased when the player is on roads to help players stick to stairs going down?")
                .define("Sticky Stairs", true);
        STICKY_GRAVITY_BOOST = BUILDER.comment("How much should the Roads increase player gravity? Only has an effect if Sticky Stairs is true. (Default is 0.12. Requires Restart.)")
                .define("Sticky Gravity Boost", 0.12D);

        BUILDER.pop();
        SPEC = BUILDER.build();
    }
}
